import calSimRank
import GenPosNegPairsbyRandom
import networkx as nx
import numpy as np
from word2gauss.embeddings import GaussianEmbedding

def test_train_batch_KL_spherical(training_data, n, dim):
	#print 'length of train data: ' + str(len(training_data))

	embed = GaussianEmbedding(n, dim,
		covariance_type='spherical',
		energy_type='KL',
		mu_max=2.0, sigma_min=0.5, sigma_max=1.0, eta=0.1, Closs=1.0
	)

	for k in xrange(0, len(training_data), 100):
		embed.train_batch(training_data[k:(k+100)])

	return embed


def get_SimRank_similarity_matrix(numOfNodes):
	simDict = calSimRank.simrank(G)
	similarity = [[0.0 for i in range(numOfNodes)] for j in range(numOfNodes)]
	for key, val in simDict.items():
		for k, v in val.items():
			similarity[key][k] = v

	return similarity


def get_RoleSim_similarity_matrix(numOfNodes, infile):
	similarity = [[0.0 for i in range(numOfNodes)] for j in range(numOfNodes)]
	fin = open(infile, 'r')
	idx = 0
	for line in fin.readlines():
		tmp = line.strip().split(',')
		for i in range(numOfNodes):
			similarity[idx][i] = float(tmp[i])
		idx += 1
	fin.close()
	return similarity



fh = open("barbell.edgelist", 'rb')
G = nx.read_edgelist(fh, nodetype=int)
fh.close()

numOfNodes = len(G.nodes())
similarity = get_RoleSim_similarity_matrix(numOfNodes, 'barbell.rolesim.sim')

training_data = np.asarray(GenPosNegPairsbyRandom.genPosNegPairs(similarity, 10, 10), dtype=np.uint32)
embed = test_train_batch_KL_spherical(training_data, numOfNodes, 2)

embedding = embed.mu
x = embedding[:,0]
y = embedding[:,1]
'''
z = [i for i in range(77)]

fig, ax = plt.subplots()
ax.scatter(x, y)

for i, txt in enumerate(z):
    ax.annotate(txt, (x[i],y[i]))
plt.savefig('lesmis.png')
#plt.show()
'''

for i in range(numOfNodes):
	print str(x[i]) + ' ',
print 
for i in range(numOfNodes):
	print str(y[i]) + ' ',