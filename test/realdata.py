import calSimRank
import GenPosNegPairsbyRandom
import networkx as nx
import numpy as np
from word2gauss.embeddings import GaussianEmbedding

def test_train_batch_EL_spherical(train_data, n, dim):
	#print 'length of train data: ' + str(len(training_data))

	embed = GaussianEmbedding(n, dim,
		covariance_type='spherical',
		energy_type='IP',
		mu_max=2.0, sigma_min=0.5, sigma_max=1.0, eta=0.1, Closs=1.0
	)

	for k in xrange(0, len(training_data), 100):
		embed.train_batch(training_data[k:(k+100)])

	return embed
	
def test_train_batch_EL_diagonal(train_data, n, dim):
	#print 'length of train data: ' + str(len(training_data))

	embed = GaussianEmbedding(n, dim,
		covariance_type='diagonal',
		energy_type='IP',
		mu_max=2.0, sigma_min=0.5, sigma_max=1.0, eta=0.1, Closs=1.0
	)

	for k in xrange(0, len(training_data), 100):
		embed.train_batch(training_data[k:(k+100)])

	return embed

def test_train_batch_KL_spherical(train_data, n, dim):
	#print 'length of train data: ' + str(len(training_data))

	embed = GaussianEmbedding(n, dim,
		covariance_type='spherical',
		energy_type='KL',
		mu_max=2.0, sigma_min=0.5, sigma_max=1.0, eta=0.1, Closs=1.0
	)

	for k in xrange(0, len(training_data), 100):
		embed.train_batch(training_data[k:(k+100)])

	return embed


def get_SimRank_similarity_matrix(numOfNodes):
	simDict = calSimRank.simrank(G)
	similarity = [[0.0 for i in range(numOfNodes)] for j in range(numOfNodes)]
	for key, val in simDict.items():
		for k, v in val.items():
			similarity[key][k] = v

	return similarity

def test_train_batch_KL_diagonal(train_data, n, dim):
	#print 'length of train data: ' + str(len(training_data))

	embed = GaussianEmbedding(n, dim,
		covariance_type='diagonal',
		energy_type='KL',
		mu_max=2.0, sigma_min=0.5, sigma_max=1.0, eta=0.1, Closs=1.0
	)

	for k in xrange(0, len(training_data), 100):
		embed.train_batch(training_data[k:(k+100)])

	return embed


def get_RoleSim_similarity_matrix(numOfNodes, infile):
	similarity = [[0.0 for i in range(numOfNodes)] for j in range(numOfNodes)]
	fin = open(infile, 'r')
	idx = 0
	for line in fin.readlines():
		tmp = line.strip().split(',')
		for i in range(numOfNodes):
			similarity[idx][i] = float(tmp[i])
		idx += 1
	fin.close()
	return similarity


numOfNodes = 6551
print numOfNodes
similarity = get_RoleSim_similarity_matrix(numOfNodes, '/home/ypei1/Embedding/NewData/advogato.simrank.sim')
training_data = np.asarray(GenPosNegPairsbyRandom.genPosNegPairs(similarity, 100, 10), dtype=np.uint32)

embed = test_train_batch_KL_spherical(training_data, numOfNodes, 128)

embedding = embed.mu
covar = embed.sigma

print (embedding.shape)
np.savetxt('/home/ypei1/Embedding/NewData/advogato.sr.emb', embedding)
np.savetxt('/home/ypei1/Embedding/NewData/advogato.sr.var', covar)

'''
embed = test_train_batch_EL_diagonal(training_data, numOfNodes, 128)
embedding = embed.mu
covar = embed.sigma

print (embedding.shape)
print type(embedding)
np.savetxt('emb/hamsterster.eld.emb', embedding)
np.savetxt('emb/hamsterster.eld.var', covar)
'''

'''
x = embedding[:,0]
y = embedding[:,1]

for i in range(numOfNodes):
	print str(x[i]) + ' ',
print 
for i in range(numOfNodes):
	print str(y[i]) + ' ',
'''